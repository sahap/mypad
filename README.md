# Microservices

One of the important lessons I have learned designing Microservices is that it demands a deep understanding of distributed systems, business knowledge and without this knowledge, it is likely to end up in doing the wrong design and communication between Microservices.



### Few common mistakes people do when designing Microservices

> 1. Spring Boot does not mean you are working with Microservices.
> 2. Using containers (Docker) does not mean you are adopting Microservices.
> 3. Multiple services, share code directly within the project,  then you are not doing Microservices.
> 4. Many services are performing a single task, could potentially lead to dependency, then it is not Microservices.
> 5. Multiple Services are running in the same JVM and accessing the same data source, then you are not doing Microservices.


### Best practices while designing Microservices

> 1. It is Not mandatory for Microservices to communicate via REST APIs. The concept of APIs as communication, that could be implemented in different ways.
> 2. How many Microservices should I divide the business capabilities into thinking in terms of business component design?
> 3. Partitioning the data into different services with their own database solution.
> 4. How data is synchronized between Microservices for redundant data or data consistency.
> 5. Selection of right interfaces, channels, and middleware for communications and interaction between services.
> 6. Business requirement of Security, Reliability, Performance, and Efficiency.
> 7. Tools and technologies to be used for individual Microservices considering cost. 


Before starting any codebase development you should be familier with the following terms.

Term          | Definition
------------- | -----------
Service |    A logical component that provides functionality through an interface
Service Instance |   A runtime instantiation of service, often a set of containers
API |   A programmatic interface designed to be consumed via HTTP
API Endpoint |  A network-addressable location within a runtime environment where an API can be accessed
API Request |   A message sent to an API endpoint that triggers a services execution
API Response |  A message sent to communicate the result of a services execution
API Consumer |  A service that sends API requests and receives API responses
API Provider |   A service that receives API requests and sends API responses; a single service can play both the API consumer and API provider role
API Intermediary |   A component that sits in the API request path from API consumer to API provider; API gateways and service proxies are common API intermediaries


Please find the below Microservices architecture used one of my projects. 

![Microservoces](img/microservices.png)






 

#### All my private projects seemed to have disappeared. Project will be published once stable ;) ###